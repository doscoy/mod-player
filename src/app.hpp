#pragma once

#include <SDL.h>

#include "imgui.h"
#include "imfilebrowser.h"

#include "player.hpp"
#include "player_viewer.hpp"
#include "data.hpp"
#include "data_viewer.hpp"

namespace ModMusic {

class App {
 public:
  App();
  void Run();

 private:
  void Process();
  void Treat4File();
  void DrawLeftPanel();
  void DrawRightPanel(const PositionData&);
  bool is_ready_{false};
  SDL_AudioDeviceID audio_device_;
  SDL_AudioSpec audio_spec_;
  SongData song_data_;
  DataViewer data_viewer_;
  Player player_;
  PlayerViewer player_viewer_;
  ImGui::FileBrowser file_dialog_;
  bool play_frag_{false};
  bool scroll_frag_{false};
};

}  // namespace MyController
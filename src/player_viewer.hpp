#pragma once

#include "player.hpp"

namespace ModMusic {

class PlayerViewer {
 public:
  PlayerViewer(const Player &player) : player_(player) {}
  void DrawPlayData();
  void DrawChData();

 private:
  const Player &player_;
  
};

}  // namespace ModMusic
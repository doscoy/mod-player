#pragma once

#include <array>
#include <filesystem>
#include <vector>

#include "common.hpp"

namespace ModMusic {

struct PatternData {
  int period{0};
  int period_index{-1};
  int inst{0};
  int effect{0};
  int param{0};
};

struct RowData {
  std::array<PatternData, Num::kChLen> pattern;
};

struct FrameData {
  std::array<RowData, Num::kRowPerFrame> row;
};

struct InstrumentData {
  int len{0};
  int finetune{0};
  int volume{0};
  int start{0};
  int end{0};
  bool one_shot{false};
};

struct SongData {
  char title[Num::kTitleLen + 1];
  std::array<int, Num::kTableLen> table;
  std::array<FrameData, Num::kFrameLen> frame;
  std::array<InstrumentData, Num::kInstLen> inst;
  std::array<char[Num::kInstNameLen + 1], Num::kInstLen> inst_name;
  std::array<std::vector<std::int8_t>, Num::kInstLen> sample;
  int song_len{0};
  int ch_len{0};
};

SongData ReadModFile(const std::filesystem::path&);

}  // namespace ModMusic
#include "data.hpp"

#include <algorithm>
#include <cstdint>
#include <fstream>

namespace ModMusic {

inline std::uint8_t ReadByteData(std::ifstream& ifs) {
  std::uint8_t tmp_8{0};
  ifs.read(reinterpret_cast<char*>(&tmp_8), 1);
  return tmp_8;
}

inline std::uint16_t ReadBigWordData(std::ifstream& ifs) {
  std::uint16_t tmp_16{0};
  ifs.read(reinterpret_cast<char*>(&tmp_16), 2);
  return ((0xff00 & tmp_16) >> 8) | ((0x00ff & tmp_16) << 8);
}

void ReadTitle(std::ifstream&, SongData&);
void ReadInstrument(std::ifstream&, SongData&);
void ReadSongLength(std::ifstream&, SongData&);
void ReadTable(std::ifstream&, SongData&);
void ReadID(std::ifstream&, SongData&);
PatternData GetPatternData(std::ifstream&);
void ReadPattern(std::ifstream&, SongData&);
void ReadSample(std::ifstream&, SongData&);

SongData ReadModFile(const std::filesystem::path& path) {
  SongData data;
  std::ifstream ifs{path, std::ios::in | std::ios::binary};

  ReadTitle(ifs, data);
  ReadInstrument(ifs, data);
  ReadSongLength(ifs, data);
  ReadTable(ifs, data);
  ReadID(ifs, data);
  ReadPattern(ifs, data);
  ReadSample(ifs, data);

  return data;
}

void ReadTitle(std::ifstream& ifs, SongData& data) {
  ifs.read(data.title, Num::kTitleLen);
  data.title[Num::kTitleLen] = '\0';
}

void ReadInstrument(std::ifstream& ifs, SongData& data) {
  for (int i = 1; i < Num::kInstLen; ++i) {
    ifs.read(data.inst_name[i], Num::kInstNameLen);
    data.inst_name[i][Num::kInstNameLen] = '\0';
    data.inst[i].len = ReadBigWordData(ifs) * 2;
    data.inst[i].finetune = ReadByteData(ifs);
    data.inst[i].volume = ReadByteData(ifs);
    data.inst[i].start = ReadBigWordData(ifs) * 2;
    const int repeat_len{ReadBigWordData(ifs) * 2};
    data.inst[i].end = data.inst[i].start + repeat_len;
    data.inst[i].one_shot = repeat_len == 2;
  }
}

void ReadSongLength(std::ifstream& ifs, SongData& data) {
  data.song_len = ReadByteData(ifs);
  ifs.seekg(1, std::ios::cur);
}

void ReadTable(std::ifstream& ifs, SongData& data) {
  for (int i = 0; i < Num::kTableLen; ++i) {
    data.table[i] = ReadByteData(ifs);
  }
}

void ReadID(std::ifstream& ifs, SongData& data) {
  char id[4];

  ifs.read(id, 4);

  if (id[0] == 'M' || id[0] == '4' || id[3] == '4') {
    data.ch_len = 4;
  } else if (id[0] == '8' || id[3] == '8') {
    data.ch_len = 8;
  } else if (id[0] == '6') {
    data.ch_len = 6;
  } else {
    data.ch_len = -1;
  }
}

PatternData GetPatternData(std::ifstream& ifs) {
  std::uint32_t tmp_32{0};
  PatternData data;

  ifs.read(reinterpret_cast<char*>(&tmp_32), 4);
  data.period = ((tmp_32 & 0x0000000f) << 8) | ((tmp_32 & 0x0000ff00) >> 8);
  data.inst = (tmp_32 & 0x000000f0) | ((tmp_32 & 0x00f00000) >> 20);
  data.effect = (tmp_32 & 0x000f0000) >> 16;
  data.param = (tmp_32 & 0xff000000) >> 24;

  const auto beg_ptr{std::begin(Table::kPeriod[0])};
  const auto end_ptr{std::end(Table::kPeriod[0])};
  const auto period_ptr{std::find(beg_ptr, end_ptr, data.period)};
  if (end_ptr != period_ptr) {
    const auto index{std::distance(beg_ptr, period_ptr)};
    data.period_index = index;
  } else {
    data.period_index = -1;
  }

  return data;
}

void ReadPattern(std::ifstream& ifs, SongData& data) {
  int frame_len{0};
  for (int i = 0; i < data.song_len; ++i) {
    if (frame_len < data.table[i]) frame_len = data.table[i];
  }

  for (int frame = 0; frame <= frame_len; ++frame) {
    for (int row = 0; row < Num::kRowPerFrame; ++row) {
      for (int ch = 0; ch < data.ch_len; ++ch) {
        data.frame[frame].row[row].pattern[ch] = GetPatternData(ifs);
      }
    }
  }
}

void ReadSample(std::ifstream& ifs, SongData& data) {
  for (int i = 1; i < Num::kInstLen; ++i) {
    if (data.inst[i].len > 0) {
      data.sample[i].resize(data.inst[i].len);
      ifs.read(reinterpret_cast<char*>(&data.sample[i][0]), data.inst[i].len);
    }
  }
}

}  // namespace ModMusic
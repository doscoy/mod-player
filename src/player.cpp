#include "player.hpp"

#include <algorithm>
#include <cmath>
#include <limits>

namespace ModMusic {

void Player::SDL2AudioCallback(void* userdata, std::uint8_t* stream, int len) {
  Player* player{static_cast<Player*>(userdata)};
  std::int16_t* frames = reinterpret_cast<std::int16_t*>(stream);
  int framesize = len / sizeof(std::int16_t);
  for (int i = 0; i < framesize; i += 2) {
    player->NextSample();
    std::int16_t l{0}, r{0};
    const std::int16_t min{-128}, max{127};
    l = player->GetChData(0) + player->GetChData(3) + player->GetChData(4) +
        player->GetChData(7);
    r = player->GetChData(1) + player->GetChData(2) + player->GetChData(5) +
        player->GetChData(6);
    l = std::clamp(l, min, max) << 8;
    r = std::clamp(r, min, max) << 8;
    frames[i] = l;
    frames[i + 1] = r;
  }
}

void Player::IncrementSample() {
  for (int ch = 0; ch < data_->ch_len; ++ch) {
    buffers_[ch].Store(GetSample(ch));
  }
  if (counter_.GetTickInclementFrag()) IncrementTick();
  counter_.Count();
}

void Player::IncrementTick() {
  cur_pos_ = next_pos_;
  if (cur_pos_.tick == 0 && cur_pos_.delay == 0) ReadRow();
  EffectTreat();
  ++next_pos_.tick;
  if (next_pos_.tick >= counter_.GetTPD() && cur_pos_.delay == 0)
    IncrementRow();
  if (next_pos_.delay > 0) {
    --next_pos_.delay;
  } else {
    next_pos_.delay = 0;
  }
}

void Player::IncrementRow() {
  next_pos_.tick = 0;
  cur_pos_.row = next_pos_.row;
  ++next_pos_.row;
  if (next_pos_.row >= Num::kRowPerFrame) IncrementFrame();
}

void Player::IncrementFrame() {
  next_pos_.row = 0;
  next_pos_.loop_point = 0;
  cur_pos_.table = next_pos_.table;
  cur_pos_.frame = next_pos_.frame;
  IncrementTable();
}

void Player::IncrementTable() {
  ++next_pos_.table;
  if (next_pos_.table >= data_->song_len) {
    next_pos_.table = 0;
    ++next_pos_.time;
  }
  next_pos_.frame = data_->table[next_pos_.table];
}

std::int8_t Player::GetSample(const int ch) {
  if (channels_[ch].sample_pos < 0 || channels_[ch].inst == 0) return 0;
  const int inst{channels_[ch].inst};
  const int pos{channels_[ch].sample_pos};
  const int volume{channels_[ch].volume};
  const std::int8_t value{data_->sample[inst][pos]};

  SamplePosTreat(ch, inst);

  return value * volume / Num::kMaxVolume;
}

void Player::SamplePosTreat(const int ch, const int inst) {
  channels_[ch].period_counter += counter_.GetCpuCount();
  if (channels_[ch].period_counter >= channels_[ch].period) {
    channels_[ch].period_counter %= channels_[ch].period;
    ++channels_[ch].sample_pos;
    if (data_->inst[inst].one_shot) {
      if (channels_[ch].sample_pos >= data_->inst[inst].len) {
        channels_[ch].sample_pos = -1;
        channels_[ch].period_counter = 0;
      }
    } else {
      if (channels_[ch].sample_pos >= data_->inst[inst].end) {
        channels_[ch].sample_pos = data_->inst[inst].start;
      }
    }
  }
}

void Player::ReadRow() {
  for (int ch = 0; ch < data_->ch_len; ++ch) {
    const auto pattern{
        data_->frame[cur_pos_.frame].row[cur_pos_.row].pattern[ch]};
    if (pattern.inst != 0) {
      channels_[ch].inst = pattern.inst;
      channels_[ch].volume = data_->inst[pattern.inst].volume;
      channels_[ch].finetune = data_->inst[pattern.inst].finetune;
      channels_[ch].sample_pos = data_->inst[pattern.inst].start;
    }
    if (pattern.period != 0) {
      channels_[ch].prev_period = channels_[ch].period;
      const int finetune{channels_[ch].finetune};
      const int index{pattern.period_index};
      channels_[ch].period = Table::kPeriod[finetune][index];
      channels_[ch].period_index = pattern.period_index;
      channels_[ch].vibrato.counter = 0;
    }
    channels_[ch].effect = pattern.effect;
    channels_[ch].param = pattern.param;
    channels_[ch].tremolo.counter = 0;
  }
}

void Player::EffectTreat() {
  for (int ch = 0; ch < data_->ch_len; ++ch) {
    if (channels_[ch].effect == 0 && channels_[ch].param == 0) continue;
    switch (channels_[ch].effect) {
      case 0x0:
        Effect0(channels_[ch]);
        break;
      case 0x1:
        Effect1(channels_[ch]);
        break;
      case 0x2:
        Effect2(channels_[ch]);
        break;
      case 0x3:
        Effect3(channels_[ch]);
        break;
      case 0x4:
        Effect4(channels_[ch]);
        break;
      case 0x5:
        Effect5(channels_[ch]);
        break;
      case 0x6:
        Effect6(channels_[ch]);
        break;
      case 0x7:
        Effect7(channels_[ch]);
        break;
      case 0x8:
        break;
      case 0x9:
        Effect9(channels_[ch]);
        break;
      case 0xA:
        EffectA(channels_[ch]);
        break;
      case 0xB:
        EffectB(channels_[ch]);
        break;
      case 0xC:
        EffectC(channels_[ch]);
        break;
      case 0xD:
        EffectD(channels_[ch]);
        break;
      case 0xE:
        EffectE(channels_[ch]);
        break;
      case 0xF:
        EffectF(channels_[ch]);
        break;
    }
  }
}

void Player::Effect0(ChStatus& ch) {
  if (ch.period_index < 0) return;
  const int x{(ch.param & 0xf0) >> 4};
  const int y{ch.param & 0xf};
  int index{0};
  switch (cur_pos_.tick % 3) {
    case 1:
      index = std::clamp(ch.period_index + x, 0, Table::kLen - 1);
      break;
    case 2:
      index = std::clamp(ch.period_index + y, 0, Table::kLen - 1);
      break;
    default:
      index = ch.period_index;
      break;
  }
  ch.period = Table::kPeriod[ch.finetune][index];
}

void Player::Effect1(ChStatus& ch) {
  const int target{ch.period - ch.param};
  ch.period = std::clamp(target, Num::kSliderMin, Num::kSliderMax);
}

void Player::Effect2(ChStatus& ch) {
  const int target{ch.period + ch.param};
  ch.period = std::clamp(target, Num::kSliderMin, Num::kSliderMax);
}

void Player::Effect3(ChStatus& ch) {
  if (ch.period_index < 0) return;
  if (ch.prev_period == Table::kPeriod[ch.finetune][ch.period_index]) return;
  if (ch.glissando) {
    Effect3Glissando(ch);
  } else {
    Effect3Smooth(ch);
  }
}

void Player::Effect3Smooth(ChStatus& ch) {
  const int tgt_period{Table::kPeriod[ch.finetune][ch.period_index]};
  if (tgt_period > ch.prev_period) {
    ch.prev_period += ch.param;
    if (tgt_period < ch.prev_period) {
      ch.prev_period = tgt_period;
    }
  } else {
    ch.prev_period -= ch.param;
    if (tgt_period > ch.prev_period) {
      ch.prev_period = tgt_period;
    }
  }
  ch.period = ch.prev_period;
}

void Player::Effect3Glissando(ChStatus& ch) {
  const int tgt_period{Table::kPeriod[ch.finetune][ch.period_index]};
  if (tgt_period > ch.prev_period) {
    const int index{std::clamp(ch.period_index - 1, 0, Table::kLen - 1)};
    const int next_period{Table::kPeriod[ch.finetune][index]};
    ch.prev_period += ch.param;
    if (tgt_period < ch.prev_period) {
      ch.prev_period = tgt_period;
      ch.period = ch.prev_period;
    } else if (next_period < ch.prev_period) {
      ch.period = next_period;
    }
  } else {
    const int index{std::clamp(ch.period_index + 1, 0, Table::kLen - 1)};
    const int next_period{Table::kPeriod[ch.finetune][index]};
    ch.prev_period -= ch.param;
    if (tgt_period > ch.prev_period) {
      ch.prev_period = tgt_period;
      ch.period = ch.prev_period;
    } else if (next_period > ch.prev_period) {
      ch.period = next_period;
    }
  }
}

void Player::Effect4(ChStatus& ch) {
  ch.vibrato.SetParameter(ch.param);
  if (ch.vibrato.Disable()) return;

  const int cur_period{Table::kPeriod[ch.finetune][ch.period_index]};
  const int sign{ch.vibrato.counter < 32 ? 1 : -1};
  const int index{std::clamp(ch.period_index + sign, 0, Table::kLen - 1)};
  const int next_period{Table::kPeriod[ch.finetune][index]};
  const int dif{sign > 0 ? next_period - cur_period : cur_period - next_period};
  const int wav_data{ch.vibrato.GetWaveValue()};

  ch.period = cur_period + (ch.vibrato.amp * wav_data * dif) / 256;
  ch.vibrato.Count();
}

void Player::Effect5(ChStatus& ch) {
  Effect3(ch);
  EffectA(ch);
}

void Player::Effect6(ChStatus& ch) {
  Effect4(ch);
  EffectA(ch);
}

void Player::Effect7(ChStatus& ch) {
  ch.tremolo.SetParameter(ch.param);
  if (ch.tremolo.Disable()) return;

  const int wav_data{ch.vibrato.GetWaveValue()};
  const int base_volume{data_->inst[ch.inst].volume};
  const int volume{base_volume + (ch.tremolo.amp * wav_data) / 16};

  ch.volume = std::clamp(volume, 0, Num::kMaxVolume);
  ch.tremolo.Count();
}

void Player::Effect9(ChStatus& ch) {
  int x{(ch.param & 0xf0) >> 4};
  int y{ch.param & 0xf};
  ch.sample_pos = x * 4096 + y * 256;
  EffectReset(ch);
}

void Player::EffectA(ChStatus& ch) {
  int x{(ch.param & 0xf0) >> 4};
  int y{ch.param & 0xf};
  if (x != 0) {
    ch.volume = std::clamp(ch.volume + x, 0, Num::kMaxVolume);
  } else if (y != 0) {
    ch.volume = std::clamp(ch.volume - y, 0, Num::kMaxVolume);
  }
}

void Player::EffectB(ChStatus& ch) {
  next_pos_.row = Num::kRowPerFrame;
  next_pos_.table = std::clamp(ch.param, 0, Num::kTableLen - 1) - 1;
  if (next_pos_.table < cur_pos_.table) ++next_pos_.time;
  EffectReset(ch);
}

void Player::EffectC(ChStatus& ch) {
  ch.volume = std::clamp(ch.param, 0, Num::kMaxVolume);
}

void Player::EffectD(ChStatus& ch) {
  int x{(ch.param & 0xf0) >> 4};
  int y{ch.param & 0xf};
  int z{x * 10 + y};
  next_pos_.row = std::clamp(z, 0, Num::kRowPerFrame - 1) - 1;
  IncrementTable();
  EffectReset(ch);
}

void Player::EffectE(ChStatus& ch) {
  int x{(ch.param & 0xf0) >> 4};
  int y{ch.param & 0xf};
  switch (x) {
    case 0x0:
      break;  // Amiga hardware filter
    case 0x1:
      EffectE1(ch, y);
      break;
    case 0x2:
      EffectE2(ch, y);
      break;
    case 0x3:
      EffectE3(ch, y);
      break;
    case 0x4:
      EffectE4(ch, y);
      break;
    case 0x5:
      EffectE5(ch, y);
      break;
    case 0x6:
      EffectE6(ch, y);
      break;
    case 0x7:
      EffectE7(ch, y);
      break;
    case 0x8:
      break;
    case 0x9:
      EffectE9(ch, y);
      break;
    case 0xA:
      EffectEA(ch, y);
      break;
    case 0xB:
      EffectEB(ch, y);
      break;
    case 0xC:
      EffectEC(ch, y);
      break;
    case 0xD:
      EffectED(ch, y);
      break;
    case 0xE:
      EffectEE(ch, y);
      break;
    case 0xF:
      EffectEF(ch, y);
      break;
  }
}

void Player::EffectE1(ChStatus& ch, const int param) {
  int target{ch.period - param};
  ch.period = std::clamp(target, Num::kSliderMin, Num::kSliderMax);
  EffectReset(ch);
}

void Player::EffectE2(ChStatus& ch, const int param) {
  int target{ch.period + param};
  ch.period = std::clamp(target, Num::kSliderMin, Num::kSliderMax);
  EffectReset(ch);
}

void Player::EffectE3(ChStatus& ch, const int param) { ch.glissando = param; }

void Player::EffectE4(ChStatus& ch, const int param) {
  ch.vibrato.SetWave(param);
}

void Player::EffectE5(ChStatus& ch, const int param) {
  ch.finetune = param;
  EffectReset(ch);
}

void Player::EffectE6(ChStatus& ch, const int param) {
  if (param == 0) {
    next_pos_.loop_point = cur_pos_.row;
  } else {
    if (next_pos_.loop_count < param && next_pos_.loop_point >= 0) {
      ++next_pos_.loop_count;
      next_pos_.row = cur_pos_.loop_point;
    } else {
      next_pos_.loop_point = -1;
      next_pos_.loop_count = 0;
    }
  }
  EffectReset(ch);
}

void Player::EffectE7(ChStatus& ch, const int param) {
  ch.tremolo.SetWave(param);
}

void Player::EffectE9(ChStatus& ch, const int param) {
  if (cur_pos_.tick < param) ch.sample_pos = 0;
}

void Player::EffectEA(ChStatus& ch, const int param) {
  ch.volume = std::clamp(ch.volume + param, 0, Num::kMaxVolume);
  EffectReset(ch);
}

void Player::EffectEB(ChStatus& ch, const int param) {
  ch.volume = std::clamp(ch.volume - param, 0, Num::kMaxVolume);
  EffectReset(ch);
}

void Player::EffectEC(ChStatus& ch, const int param) {
  if (cur_pos_.tick >= param) ch.volume = 0;
}

void Player::EffectED(ChStatus& ch, const int param) {
  if (cur_pos_.tick < param) {
    ch.volume = 0;
    ch.sample_pos = 0;
  }
}

void Player::EffectEE(ChStatus& ch, const int param) {
  next_pos_.delay = (param + 1) * counter_.GetTPD();
  EffectReset(ch);
}

void Player::EffectEF(ChStatus& ch, [[maybe_unused]] const int param) {
  // invert sample loop
  EffectReset(ch);
}

void Player::EffectF(ChStatus& ch) {
  if (ch.param <= Num::kEffectFThr) {
    counter_.SetTPD(ch.param);
  } else {
    counter_.SetBPM(ch.param);
  }
  EffectReset(ch);
}

}  // namespace ModMusic
#pragma once

#include "common.hpp"

namespace ModMusic {

class Counter {
 public:
  Counter() = delete;
  Counter(const int, const double, const int);
  int GetSampleRate() const { return sample_rate_; }
  int GetSPT() const { return spt_; }
  bool GetTickInclementFrag() const { return tick_increment_; }
  int GetTPD() const { return tpd_; }
  int GetBPM() const { return bpm_; }
  int GetDividedTick(const int div) const;
  int GetCpuCount() const { return cpu_clock_.Get(); }
  int GetSampleCount() const { return sample_counter_; }
  void SetTPD(int);
  void SetBPM(int);
  void Count();
  void Reset(const int, const int);

 private:
  class CpuClock {
   public:
    CpuClock() = delete;
    CpuClock(const int, const double, const int);
    int Get() const { return count_int_; }
    void Count();
    void Reset();

   private:
    int byte_rate_;   // byte per cpu clock
    int cps_int_;     // cpu clock per audio sample (integer part)
    double cps_dec_;  // cpu clock per audio sample (decimal part)
    int count_int_;
    double count_dec_;
  };
  int CalcTickThr();
  int sample_rate_;
  int sample_counter_{0};
  int tpd_{Num::kDefaultTPD};  // tick per division
  int bpm_{Num::kDefaultBPM};  // beat per minits (beat = 4division & tpd = 6)
  int spt_;                    // audio sample per tick
  bool tick_increment_{false};
  CpuClock cpu_clock_;
};

// -----------------------------------------------------------------------------

inline Counter::Counter(const int sample_rate, const double cpu_clock,
                        const int cpu_byte_rate_)
    : sample_rate_(sample_rate),
      spt_(CalcTickThr()),
      cpu_clock_(sample_rate, cpu_clock, cpu_byte_rate_) {}

inline Counter::CpuClock::CpuClock(const int sample_rate, const double clock,
                                   const int byte_rate)
    : byte_rate_(byte_rate),
      cps_int_(clock / sample_rate / byte_rate_),
      cps_dec_(clock / sample_rate / byte_rate_ - cps_int_),
      count_int_(0),
      count_dec_(0.0) {}

inline int Counter::CalcTickThr() { return (60 * sample_rate_) / (24 * bpm_); }

inline int Counter::GetDividedTick(const int div) const {
  return (sample_counter_ * div) / spt_;
}

inline void Counter::SetTPD(int tpd) {
  tpd_ = tpd > 0 ? tpd : 1;
  spt_ = CalcTickThr();
}

inline void Counter::SetBPM(int bpm) {
  bpm_ = bpm;
  spt_ = CalcTickThr();
}

inline void Counter::Reset(const int tpd, const int bpm) {
  tpd_ = tpd > 0 ? tpd : 1;
  bpm_ = bpm;
  spt_ = CalcTickThr();
  sample_counter_ = 0;
  tick_increment_ = false;
  cpu_clock_.Reset();
}

inline void Counter::CpuClock::Count() {
  count_dec_ += cps_dec_;
  if (count_dec_ < 1.0) {
    count_int_ = cps_int_;
  } else {
    count_int_ = cps_int_ + 1;
    count_dec_ -= 1.0;
  }
}

inline void Counter::CpuClock::Reset() {
  count_int_ = 0;
  count_dec_ = 0.0;
}

inline void Counter::Count() {
  ++sample_counter_;
  if (sample_counter_ >= spt_) {
    sample_counter_ = 0;
    tick_increment_ = true;
  } else {
    tick_increment_ = false;
  }
  cpu_clock_.Count();
}

}  // namespace ModMusic
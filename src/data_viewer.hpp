#pragma once

#include <algorithm>
#include <iterator>

#include "common.hpp"
#include "data.hpp"
#include "imgui.h"

namespace ModMusic {

class DataViewer {
 public:
  DataViewer(const SongData& data) : data_(data) {}
  void DrawSongData(const int = -1, const bool = false);
  void DrawPatternData(const int = -1, const int = -1, const bool = false);

 private:
  void DrawTableList(const int, const bool);
  void DrawSampleList();
  void DrawSampleData() const;
  void DrawTableData(const int, const int, const bool) const;
  const SongData& data_;
  int selected_inst_{0};
  int selected_table_{0};
};

}  // namespace ModMusic
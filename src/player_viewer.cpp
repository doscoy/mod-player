#include "player_viewer.hpp"

#include <algorithm>
#include <cmath>
#include <iterator>

#include "common.hpp"
#include "imgui.h"
#include "size_def.hpp"

namespace ModMusic {

void DrawSongData(const Player&, const PositionData&);
void DrawSoundWave(const std::int8_t**, const int);
void DrawChStatus(const int, const ChStatus&);
void DrawChWave(const std::int8_t*, const int);

void PlayerViewer::DrawPlayData() {
  PositionData position{player_.GetPosition()};
  DrawSongData(player_, position);

  ImGui::Spacing();

  const std::int8_t* ch_bufffers[Num::kChLen];
  for (int ch = 0; ch < Num::kChLen; ++ch) {
    ch_bufffers[ch] = player_.GetChBuffer(ch);
  }
  DrawSoundWave(ch_bufffers, player_.GetChBufIndex(0));
}

void PlayerViewer::DrawChData() {
  for (int ch = 0; ch < Num::kChLen; ++ch) {
    char label[8];
    sprintf(label, "ch %d", ch + 1);
    ImGui::BeginChild(
        label, ImVec2(Size::kChStatusWidth, Size::kChStatusHeight), true);

    DrawChStatus(ch, player_.GetChStatus(ch));
    DrawChWave(player_.GetChBuffer(ch), player_.GetChBufIndex(ch));

    ImGui::EndChild();
    ImGui::SameLine();
  }
  ImGui::Dummy(ImVec2());
}

void DrawSongData(const Player& player, const PositionData& position) {
  ImGui::Text("tpd  : %d", player.GetTPD());
  ImGui::Text("bpm  : %d", player.GetBPM());
  ImGui::Text("time : %d", position.time);
  ImGui::Text("table: %02x", position.table);
  ImGui::Text("frame: %02x", position.frame);
  ImGui::Text("row  : %02x", position.row);
  ImGui::Text("tick : %02x", position.tick);
  ImGui::Text("smpl : %02x", player.GetSampleCount());
  ImGui::Text("l row: %02x", position.loop_point);
  ImGui::Text("l cnt: %01x", position.loop_count);
  ImGui::Text("delay: %01x", position.delay);
}

void DrawSoundWave(const std::int8_t** ch_buffers, const int buffer_position) {
  float l_buf_f[Num::kBufLen], r_buf_f[Num::kBufLen];
  for (int i = 0; i < Num::kBufLen; ++i) {
    const int l{ch_buffers[0][i] + ch_buffers[3][i] + ch_buffers[4][i] +
                ch_buffers[7][i]};
    const int r{ch_buffers[1][i] + ch_buffers[2][i] + ch_buffers[5][i] +
                ch_buffers[6][i]};
    l_buf_f[i] = std::clamp(l, -128, 127);
    r_buf_f[i] = std::clamp(r, -128, 127);
  }
  ImGui::PlotLines("", l_buf_f, Num::kBufLen, buffer_position, "L ch", -128.0f,
                   127.0f,
                   ImVec2(Size::kSongItemWidth, Size::kSongWaveHeight));
  ImGui::PlotLines("", r_buf_f, Num::kBufLen, buffer_position, "R ch", -128.0f,
                   127.0f,
                   ImVec2(Size::kSongItemWidth, Size::kSongWaveHeight));
}

void DrawChStatus(const int ch, const ChStatus& ch_status) {
  ImGui::Text("ch    : %d", ch + 1);
  ImGui::Text("prev  : %d", ch_status.prev_period);
  ImGui::Text("period: %d", ch_status.period);
  ImGui::Text("       (%d)", ch_status.period_index);
  float peri{(std::log2f(ch_status.period * 12 * 8) - std::log2f(50 * 12 * 8)) /
             (std::log2f(1800 * 12 * 8) - std::log2f(50 * 12 * 8))};
  ImGui::ProgressBar(
      peri, ImVec2(Size::kChItemWidth, Size::kChProgressBarHeight), "");
  ImGui::Text("count : %d", ch_status.period_counter);
  const int finetune{ch_status.finetune};
  const int fix_tune{finetune < 8 ? finetune : finetune - 16};
  ImGui::Text("f tune: %d", fix_tune);
  if (ch_status.sample_pos < 0) {
    ImGui::Text("s pos : ----");
  } else {
    ImGui::Text("s pos : %04x", ch_status.sample_pos);
  }
  ImGui::Text("inst  : %02x", ch_status.inst);
  ImGui::Text("effect: %01x", ch_status.effect);
  ImGui::Text("param : %02x", ch_status.param);
  ImGui::Text("volume: %02x", ch_status.volume);
  const float vol{static_cast<float>(ch_status.volume) /
                  static_cast<float>(Num::kMaxVolume)};
  ImGui::ProgressBar(
      vol, ImVec2(Size::kChItemWidth, Size::kChProgressBarHeight), "");
}

void DrawChWave(const std::int8_t* ch_buffer, const int buffer_position) {
  float ch_buffer_float[Num::kBufLen];
  for (int i = 0; i < Num::kBufLen; ++i) {
    ch_buffer_float[i] = ch_buffer[i];
  }
  ImGui::PlotLines("", ch_buffer_float, Num::kBufLen, buffer_position, "",
                   -128.0f, 127.0f,
                   ImVec2(Size::kChItemWidth, Size::kChWaveHeight));
}

}  // namespace ModMusic
#include "data_viewer.hpp"

#include "size_def.hpp"

namespace ModMusic {

namespace {

const ImVec4 kBoldColor{1.0f, 0.0f, 0.0f, 1.0f};

}

void DrawTableNote(const int, const int);
void DrawTableInst(const int);
void DrawTableEffect(const int, const int);

void DataViewer::DrawSongData(const int current_table, const bool scroll_frag) {
  ImGui::Text("title: %s", data_.title);
  ImGui::BeginChild(
      "table", ImVec2(Size::kTableListWidth, Size::kTableListHeight), true);

  DrawTableList(current_table, scroll_frag);

  ImGui::EndChild();
  ImGui::SameLine();

  ImGui::BeginChild("sample name",
                    ImVec2(Size::kSampleListWidth, Size::kSampleListHeight),
                    true);

  DrawSampleList();

  ImGui::EndChild();

  if (selected_inst_ != 0) {
    DrawSampleData();
  }
}

void DataViewer::DrawPatternData(const int current_table, const int current_row,
                                 const bool scroll_frag) {
  ImGui::BeginChild("patterns",
                    ImVec2(Size::kPatternWidth, Size::kPatternHeight), true, ImGuiWindowFlags_NoScrollbar);

  DrawTableData(current_table, current_row, scroll_frag);

  ImGui::EndChild();
}

void DataViewer::DrawTableList(const int current_table,
                               const bool scroll_frag) {
  for (int table = 0; table < data_.song_len; ++table) {
    char label[8];
    sprintf(label, "%02x %02x", table, data_.table[table]);
    if (table == current_table) {
      ImGui::PushStyleColor(ImGuiCol_Text, kBoldColor);
    }
    if (ImGui::Selectable(label, selected_table_ == table)) {
      selected_table_ = table;
    }
    if (table == current_table) {
      ImGui::PopStyleColor();
      if (scroll_frag) {
        ImGui::SetScrollHereY(0.5f);
        selected_table_ = current_table;
      }
    }
  }
}

void DataViewer::DrawSampleList() {
  for (int inst = 1; inst < Num::kInstLen; ++inst) {
    char label[4 + Num::kInstNameLen];
    sprintf(label, "%02x %s", inst, data_.inst_name[inst]);
    if (ImGui::Selectable(label, selected_inst_ == inst)) {
      selected_inst_ = inst;
    }
  }
}

void DataViewer::DrawSampleData() const {
  ImGui::SameLine();
  ImGui::BeginChild("sample property",
                    ImVec2(Size::kSampleDataWidth, Size::kSampleDataHeight),
                    true);
  ImGui::Text("length  : %d", data_.inst[selected_inst_].len);
  const int finetune{data_.inst[selected_inst_].finetune};
  const int fix_tune{finetune < 8 ? finetune : finetune - 16};
  ImGui::Text("finetune: %d", fix_tune);
  ImGui::Text("volume  : %d", data_.inst[selected_inst_].volume);
  ImGui::Text("start   : %d", data_.inst[selected_inst_].start);
  ImGui::Text("end     : %d", data_.inst[selected_inst_].end);
  ImGui::EndChild();
  if (data_.inst[selected_inst_].len > 0) {
    ImGui::SameLine();
    float* sample = new float[data_.inst[selected_inst_].len];
    for (int i = 0; i < data_.inst[selected_inst_].len; ++i) {
      sample[i] = data_.sample[selected_inst_][i];
    }
    ImGui::PlotLines("", sample, data_.inst[selected_inst_].len, 0, "", -128.0f,
                     127.0f,
                     ImVec2(Size::kSampleWaveWidth, Size::kSampleWaveHeight));
    delete[] sample;
  }
}

void DataViewer::DrawTableData(const int current_table, const int current_row,
                               const bool scroll_frag) const {
  int frame{data_.table[selected_table_]};
  for (int row = 0; row < Num::kRowPerFrame; ++row) {
    if (row == current_row && selected_table_ == current_table) {
      ImGui::PushStyleColor(ImGuiCol_Text, kBoldColor);
    }
    ImGui::Text("%02x", row);
    ImGui::SameLine();
    for (int ch = 0; ch < data_.ch_len; ++ch) {
      if (ch == 0) {
        ImGui::Dummy(ImVec2(1.0f, 0.0f));
      } else {
        ImGui::Dummy(ImVec2(Size::kChStatusWidth - 88.0f, 0.0f));
      }
      ImGui::SameLine();
      DrawTableNote(data_.frame[frame].row[row].pattern[ch].period_index,
                    data_.frame[frame].row[row].pattern[ch].period);
      ImGui::SameLine();
      DrawTableInst(data_.frame[frame].row[row].pattern[ch].inst);
      ImGui::SameLine();
      DrawTableEffect(data_.frame[frame].row[row].pattern[ch].effect,
                      data_.frame[frame].row[row].pattern[ch].param);
      ImGui::SameLine();
    }
    ImGui::Dummy(ImVec2(0, 0));
    if (row == current_row && selected_table_ == current_table) {
      ImGui::PopStyleColor();
      if (scroll_frag) {
        ImGui::SetScrollHereY(0.5f);
      }
    }
  }
}

void DrawTableNote(const int period_index, const int period) {
  if (period_index < 0) {
    if (period == 0) {
      ImGui::TextDisabled("---");
    } else {
      ImGui::Text("???");
    }
  } else {
    ImGui::Text("%s", Table::kToneName[period_index]);
  }
}

void DrawTableInst(const int inst) {
  if (inst != 0) {
    ImGui::Text("%02x", inst);
  } else {
    ImGui::TextDisabled("--");
  }
}

void DrawTableEffect(const int effect, const int parameter) {
  if (effect != 0 || parameter != 0) {
    ImGui::Text("%01x", effect);
    ImGui::SameLine();
    ImGui::Text("%02x", parameter);
  } else {
    ImGui::TextDisabled("-");
    ImGui::SameLine();
    ImGui::TextDisabled("--");
  }
}

}  // namespace ModMusic
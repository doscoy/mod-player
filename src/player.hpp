#pragma once

#include <array>
#include <cstddef>
#include <mutex>

#include "common.hpp"
#include "counter.hpp"
#include "data.hpp"

namespace ModMusic {

template <class T, std::size_t Size>
class RingBuffer {
  static_assert(((Size != 0) && !(Size & (Size - 1))),
                "ringbuffer size must be a power of 2");

 public:
  const T* GetData() const { return &data_[0]; }
  T GetCurrent() const { return data_[index_]; }
  int GetPos() const { return index_; }
  int GetSize() const { return Size; }
  void Store(const T& item) {
    index_ = (index_ + 1) & (Size - 1);
    data_[index_] = item;
  }
  void Clear() {
    for (std::size_t i = 0; i < Size; ++i) data_[i] = T{};
    index_ = Size - 1;
  }

 private:
  T data_[Size];
  int index_{Size - 1};
};

struct PositionData {
  int time{0};
  int table{0};
  int frame{0};
  int row{0};
  int tick{0};
  int loop_point{0};
  int loop_count{0};
  int delay{0};
};

class EffectStatus {
 public:
  int freq{0};
  int amp{0};
  int counter{0};
  int wave{0};
  void SetParameter(const int);
  void SetWave(const int);
  bool Disable() { return freq == 0 && amp == 0; }
  int GetWaveValue();
  void Count() { counter = (counter + freq) & (Num::kViblatoFreq - 1); }

 private:
  int Sine();
  int Saw();
  int Square();
  int Random();
};

struct ChStatus {
  int prev_period{0};
  int period{0};
  int period_index{-1};
  int period_counter{0};
  int inst{0};
  int effect{0};
  int param{0};
  int volume{0};
  int finetune{0};
  int sample_pos{0};
  int inv_loop{0};
  bool glissando{false};
  EffectStatus vibrato;
  EffectStatus tremolo;
};

class Player {
 public:
  Player(const int = 44100, const double = Num::kAmiga500ClockNTSC,
         const int = Num::kAmiga500ByteRate);
  Player(SongData*, const int = 44100, const double = Num::kAmiga500ClockNTSC,
         const int = Num::kAmiga500ByteRate);
  PositionData GetPosition() const { return cur_pos_; }
  int GetSampleRate() const { return counter_.GetSampleRate(); }
  int GetSampleCount() const { return counter_.GetSampleCount(); }
  int GetTPD() const { return counter_.GetTPD(); }
  int GetBPM() const { return counter_.GetBPM(); }
  ChStatus GetChStatus(const int ch) const { return channels_[ch]; }
  const std::int8_t* GetChBuffer(const int ch) const {
    return buffers_[ch].GetData();
  }
  int GetChBufIndex(const int ch) const { return buffers_[ch].GetPos(); }
  std::int8_t GetChData(const int ch) const {
    return buffers_[ch].GetCurrent();
  }
  void SetData(SongData*);
  void Reset();
  void NextSample();
  void NextTick();
  void NextRow();
  void NextFrame();
  static void SDL2AudioCallback(void*, std::uint8_t*, int);

 private:
  void IncrementSample();
  void IncrementTick();
  void IncrementRow();
  void IncrementFrame();
  void IncrementTable();
  std::int8_t GetSample(const int);
  void SamplePosTreat(const int, const int);
  void ReadRow();
  void EffectTreat();
  void EffectReset(ChStatus&);
  void Effect0(ChStatus&);
  void Effect1(ChStatus&);
  void Effect2(ChStatus&);
  void Effect3(ChStatus&);
  void Effect3Smooth(ChStatus&);
  void Effect3Glissando(ChStatus&);
  void Effect4(ChStatus&);
  void Effect5(ChStatus&);
  void Effect6(ChStatus&);
  void Effect7(ChStatus&);
  void Effect9(ChStatus&);
  void EffectA(ChStatus&);
  void EffectB(ChStatus&);
  void EffectC(ChStatus&);
  void EffectD(ChStatus&);
  void EffectE(ChStatus&);
  void EffectE1(ChStatus&, const int);
  void EffectE2(ChStatus&, const int);
  void EffectE3(ChStatus&, const int);
  void EffectE4(ChStatus&, const int);
  void EffectE5(ChStatus&, const int);
  void EffectE6(ChStatus&, const int);
  void EffectE7(ChStatus&, const int);
  void EffectE9(ChStatus&, const int);
  void EffectEA(ChStatus&, const int);
  void EffectEB(ChStatus&, const int);
  void EffectEC(ChStatus&, const int);
  void EffectED(ChStatus&, const int);
  void EffectEE(ChStatus&, const int);
  void EffectEF(ChStatus&, const int);
  void EffectF(ChStatus&);

  SongData* data_{nullptr};
  Counter counter_;
  PositionData cur_pos_;
  PositionData next_pos_;
  std::array<ChStatus, Num::kChLen> channels_;
  std::array<RingBuffer<std::int8_t, Num::kBufLen>, Num::kChLen> buffers_;
};

inline void EffectStatus::SetParameter(const int param) {
  const int x{(param & 0xf0) >> 4};
  const int y{param & 0xf};
  if (x != 0) freq = x;
  if (y != 0) amp = y;
}

inline void EffectStatus::SetWave(const int param) {
  switch (param) {
    case 0:
      wave = 0;
      counter = 0;
      break;
    case 1:
      wave = 1;
      counter = 0;
      break;
    case 2:
      wave = 2;
      counter = 0;
      break;
    case 3:
      wave = 3;
      counter = 0;
      break;
    case 4:
      wave = 0;
      break;
    case 5:
      wave = 1;
      break;
    case 6:
      wave = 2;
      break;
    case 7:
      wave = 3;
      break;
    default:
      wave = 0;
      counter = 0;
      break;
  }
}

inline int EffectStatus::GetWaveValue() {
  switch (wave) {
    case 0:
      return Sine();
      break;
    case 1:
      return Saw();
      break;
    case 2:
      return Square();
      break;
    case 3:
      return Random();
      break;
    default:
      return Sine();
      break;
  }
}

inline int EffectStatus::Sine() {
  if (counter < Num::kViblatoFreq / 2) {
    return Table::kEffectSine[counter];
  } else {
    const int index{counter - Num::kViblatoFreq / 2};
    return -Table::kEffectSine[index];
  }
}

inline int EffectStatus::Saw() { return (Num::kViblatoFreq - counter * 2) / 4; }

inline int EffectStatus::Square() {
  return counter < Num::kViblatoFreq / 2 ? 16 : -16;
}

inline int EffectStatus::Random() { return Sine(); }

inline Player::Player(const int sample_rate, const double cpu_clock,
                      const int cpu_byte_rate)
    : counter_(sample_rate, cpu_clock, cpu_byte_rate) {}

inline Player::Player(SongData* data, const int sample_rate,
                      const double cpu_clock, const int cpu_byte_rate)
    : data_(data), counter_(sample_rate, cpu_clock, cpu_byte_rate) {
  Reset();
}

inline void Player::Reset() {
  if (data_ != nullptr) {
    counter_.Reset(Num::kDefaultTPD, Num::kDefaultBPM);
    for (ChStatus& ch : channels_) ch = ChStatus{};
    for (auto& buf : buffers_) buf.Clear();
    cur_pos_ = PositionData{};
    cur_pos_.frame = data_->table[0];
    next_pos_ = cur_pos_;
    next_pos_.tick = 1;
    ReadRow();
  }
}

inline void Player::SetData(SongData* data) {
  if (data != nullptr) {
    data_ = data;
    Reset();
  }
}

inline void Player::NextSample() {
  if (data_ == nullptr) return;
  IncrementSample();
}

inline void Player::NextTick() {
  if (data_ == nullptr) return;
  do {
    IncrementSample();
  } while (!counter_.GetTickInclementFrag());
}

inline void Player::NextRow() {
  if (data_ == nullptr) return;
  do {
    NextTick();
  } while (cur_pos_.tick != 0);
}

inline void Player::NextFrame() {
  if (data_ == nullptr) return;
  do {
    NextRow();
  } while (cur_pos_.row != 0);
}

inline void Player::EffectReset(ChStatus& ch) {
  ch.effect = 0;
  ch.param = 0;
}

}  // namespace ModMusic
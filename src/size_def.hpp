#pragma once

#include "common.hpp"

namespace ModMusic {

namespace Size {

inline constexpr float kWindowWidth{1024.0f};
inline constexpr float kWindowHeight{720.0f};
inline constexpr float kMainMenuBarHeight{20.0f};

inline constexpr float kPaddingWidth{8.0f};
inline constexpr float kPaddingHeight{8.0f};

inline constexpr float kSpacingWidth{8.0f};
inline constexpr float kSpacingHeight{4.0f};

inline constexpr float kTextHeight{13.0f};

inline constexpr int kTableListRow{5};
inline constexpr float kTableListWidth{115.0f};
inline constexpr float kTableListHeight{
    (kTextHeight + kSpacingHeight) * kTableListRow + kSpacingHeight * 3};

inline constexpr float kSampleListWidth{250.0f};
inline constexpr float kSampleListHeight{kTableListHeight};

inline constexpr float kSampleDataWidth{150.0f};
inline constexpr float kSampleDataHeight{kTableListHeight};

inline constexpr float kSampleWaveWidth{kWindowWidth - kTableListWidth -
                                        kSampleListWidth - kSampleDataWidth -
                                        kPaddingWidth * 2 - kSpacingWidth * 3};
inline constexpr float kSampleWaveHeight{kTableListHeight};

inline constexpr float kSongDataWidth{kTableListWidth};
inline constexpr float kSongDataHeight{kWindowHeight - kMainMenuBarHeight -
                                       kTextHeight - kTableListHeight -
                                       kPaddingHeight * 2 - kSpacingHeight * 2};
inline constexpr float kSongItemWidth{kSongDataWidth - kSpacingWidth * 2};
inline constexpr float kSongWaveHeight{kSongItemWidth};

inline constexpr int kPatternRow{15};
inline constexpr float kPatternWidth{kWindowWidth - kTableListWidth -
                                     kSpacingWidth * 3};
inline constexpr float kPatternHeight{
    (kTextHeight + kSpacingHeight) * kPatternRow + kSpacingHeight};

inline constexpr float kChStatusWidth{100.0f};
inline constexpr float kChStatusHeight{
    kWindowHeight - kMainMenuBarHeight - kTextHeight - kTableListHeight -
    kPatternHeight - kPaddingHeight * 2 - kSpacingHeight * 3};
inline constexpr float kChItemWidth{kChStatusWidth - kSpacingWidth * 2};
inline constexpr float kChProgressBarHeight{kTextHeight};
inline constexpr float kChWaveHeight{
    kChStatusHeight - (kTextHeight + kSpacingHeight) * 13 - kSpacingHeight * 4};
inline constexpr float kChDummyWidth{
    kWindowWidth - kTableListWidth - kChStatusWidth * Num::kChLen -
    kPaddingWidth * 2 - kSpacingWidth * (2 + Num::kChLen - 1)};

}  // namespace Size

}  // namespace ModMusic
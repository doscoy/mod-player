#include "imgui_renderer.hpp"

#include <string>

#import <Metal/Metal.h>
#import <QuartzCore/QuartzCore.h>
#include <SDL.h>
#include <stdio.h>

#include "imgui.h"
#include "imgui_impl_metal.h"
#include "imgui_impl_sdl.h"

namespace ImGuiRnderer {

static SDL_Window* kWindow = nullptr;
static SDL_Renderer* kRenderer = nullptr;
static CAMetalLayer* kLayer = nullptr;
static id<MTLCommandQueue> kCommandQueue = nullptr;
static MTLRenderPassDescriptor* kRenderPassDescriptor = nullptr;
static bool kDone = false;

bool Init(const std::string& window_title, const int window_x, const int window_y,
          const float clear_r, const float clear_g, const float clear_b, const float clear_a) {
  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO();
  (void)io;
  io.IniFilename = NULL;

  // Setup style
  ImGui::StyleColorsDark();

  // Setup SDL
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
    printf("Error: %s\n", SDL_GetError());
    return false;
  }

  // Inform SDL that we will be using metal for rendering. Without this hint
  // initialization of metal renderer may fail.
  SDL_SetHint(SDL_HINT_RENDER_DRIVER, "metal");

  // enable screensaver
  // if sdl does not exit successfully,
  // the zombie process that disables the screen saver will continue to remain.
  SDL_SetHint(SDL_HINT_VIDEO_ALLOW_SCREENSAVER, "1");

  kWindow =
      SDL_CreateWindow(window_title.c_str(), SDL_WINDOWPOS_CENTERED,
                       SDL_WINDOWPOS_CENTERED, window_x, window_y,
                       SDL_WINDOW_ALLOW_HIGHDPI);
  if (kWindow == NULL) {
    printf("Error creating window: %s\n", SDL_GetError());
    return false;
  }

  kRenderer = SDL_CreateRenderer(
      kWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (kRenderer == NULL) {
    printf("Error creating renderer: %s\n", SDL_GetError());
    return false;
  }

  // Setup Platform/Renderer backends
  kLayer = (__bridge CAMetalLayer*)SDL_RenderGetMetalLayer(kRenderer);
  kLayer.pixelFormat = MTLPixelFormatBGRA8Unorm;
  ImGui_ImplMetal_Init(kLayer.device);
  ImGui_ImplSDL2_InitForMetal(kWindow);

  kCommandQueue = [kLayer.device newCommandQueue];
  kRenderPassDescriptor = [MTLRenderPassDescriptor new];
  kRenderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(
      clear_r, clear_g, clear_b, clear_a);
  return true;
}

bool IsDone() { return kDone; }

void NewFrame() {
  SDL_Event event;
  while (SDL_PollEvent(&event)) {
    ImGui_ImplSDL2_ProcessEvent(&event);
    if (event.type == SDL_QUIT) kDone = true;
    if (event.type == SDL_WINDOWEVENT &&
        event.window.event == SDL_WINDOWEVENT_CLOSE &&
        event.window.windowID == SDL_GetWindowID(kWindow))
      kDone = true;
  }

  // Start the Dear ImGui frame
  ImGui_ImplSDL2_NewFrame(kWindow);
  ImGui::NewFrame();
}

void Render() {
  @autoreleasepool {
    int width, height;
    SDL_GetRendererOutputSize(kRenderer, &width, &height);
    kLayer.drawableSize = CGSizeMake(width, height);
    id<CAMetalDrawable> drawable = [kLayer nextDrawable];

    id<MTLCommandBuffer> commandBuffer = [kCommandQueue commandBuffer];
    kRenderPassDescriptor.colorAttachments[0].texture = drawable.texture;
    kRenderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
    kRenderPassDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
    id<MTLRenderCommandEncoder> renderEncoder = [commandBuffer
        renderCommandEncoderWithDescriptor:kRenderPassDescriptor];
    [renderEncoder pushDebugGroup:@"ImGuiRenderer"];

    // Rendering
    ImGui_ImplMetal_NewFrame(kRenderPassDescriptor);
    ImGui::Render();
    ImGui_ImplMetal_RenderDrawData(ImGui::GetDrawData(), commandBuffer,
                                   renderEncoder);

    [renderEncoder popDebugGroup];
    [renderEncoder endEncoding];

    [commandBuffer presentDrawable:drawable];
    [commandBuffer commit];
  }
}

void ShutDown() {
  // Cleanup
  ImGui_ImplMetal_Shutdown();
  ImGui_ImplSDL2_Shutdown();
  ImGui::DestroyContext();
  SDL_DestroyRenderer(kRenderer);
  SDL_DestroyWindow(kWindow);
  SDL_Quit();
}

void ShowDemos() {
  static bool show_imgui_demo = false;

  if (ImGui::BeginMainMenuBar()) {
    if (ImGui::BeginMenu("demo")) {
      ImGui::MenuItem("imgui demo", NULL, &show_imgui_demo);
      ImGui::EndMenu();
    }
    ImGui::EndMainMenuBar();
  }
  if (show_imgui_demo) ImGui::ShowDemoWindow(&show_imgui_demo);
}

}  // namespace ImGuiRnderer
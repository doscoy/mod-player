project(imgui_renderer)

find_library(Cocoa 
  NAMES Cocoa
)
find_library(IOKit
  NAMES IOKit
)
find_library(GameController
  NAMES GameController
)
find_library(CoreHaptics
  NAMES CoreHaptics
)
find_library(ForceFeedback
  NAMES ForceFeedback
)
find_library(AudioToolbox
  NAMES AudioToolbox
)
find_library(Carbon
  NAMES Carbon
)
find_library(Metal
  NAMES Metal
)
find_library(CoreVideo
  NAMES CoreVideo
)
find_library(CoreAudio
  NAMES CoreAudio
)
find_library(iconv
  NAMES iconv
)
find_library(SDL2
  NAMES libSDL2.a
  PATHS ${USER_LOCAL}/lib
)

SET(Libs
  imgui
  ${SDL2}
  ${Cocoa}
  ${IOKit}
  ${GameController}
  ${CoreHaptics}
  ${ForceFeedback}
  ${AudioToolbox}
  ${Carbon}
  ${Metal}
  ${CoreVideo}
  ${CoreAudio}
  ${iconv}
)


add_library(${PROJECT_NAME}
  STATIC
    imgui_renderer_sdl2_metal.mm
    ${CMAKE_CURRENT_SOURCE_DIR}/../extern/imgui/backends/imgui_impl_sdl.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/../extern/imgui/backends/imgui_impl_metal.mm
)

target_link_libraries(${PROJECT_NAME}
  PUBLIC
    ${Libs}
)

target_include_directories(${PROJECT_NAME}
  PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${USER_LOCAL}/include/SDL2
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/../extern/imgui
    ${CMAKE_CURRENT_SOURCE_DIR}/../extern/imgui/backends
)

target_compile_options(${PROJECT_NAME}
  PRIVATE
    -fobjc-arc ## To avoid the error "cannot create __weak reference in file using manual reference counting"
)
#pragma once

#include <string>

namespace ImGuiRnderer {
bool Init(const std::string& = "no title", const int = 1280, const int = 720,
          const float = 0.0f, const float = 0.0f, const float = 0.0f, const float = 1.0f);
bool IsDone();
void NewFrame();
void Render();
void ShutDown();
void ShowDemos();
}  // namespace ImGuiRnderer

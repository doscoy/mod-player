#include "app.hpp"

#include <cmath>

#include "imgui_renderer.hpp"
#include "size_def.hpp"

namespace ModMusic {

namespace {
constexpr char kWindowTitle[]{"mod player"};

const ImVec2 kInnerWindowPos{0, Size::kMainMenuBarHeight};
const ImVec2 kInnerWindowSize{Size::kWindowWidth,
                              Size::kWindowHeight - Size::kMainMenuBarHeight};

constexpr auto kInnerWindowFlag{
    ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings |
    ImGuiWindowFlags_NoBringToFrontOnFocus};

}  // namespace

App::App()
    : data_viewer_(song_data_), player_(&song_data_), player_viewer_(player_) {
  is_ready_ =
      ImGuiRnderer::Init(kWindowTitle, Size::kWindowWidth, Size::kWindowHeight);
  SDL_AudioSpec desired_spec;
  desired_spec.freq = player_.GetSampleRate();
  desired_spec.format = AUDIO_S16SYS;
  desired_spec.channels = 2;
  desired_spec.silence = 0;
  desired_spec.samples = 512;
  desired_spec.padding = 0;
  desired_spec.size = 0;
  desired_spec.callback = ModMusic::Player::SDL2AudioCallback;
  desired_spec.userdata = &player_;
  audio_device_ = SDL_OpenAudioDevice(NULL, 0, &desired_spec, &audio_spec_, 0);
  is_ready_ = is_ready_ && (audio_device_ != 0);
  file_dialog_.SetTypeFilters({".mod", ".MOD"});
}

void App::Run() {
  if (!is_ready_) return;

  while (!ImGuiRnderer::IsDone()) {
    ImGuiRnderer::NewFrame();
    // ImGuiRnderer::ShowDemos();
    ImGui::SetNextWindowPos(kInnerWindowPos);
    ImGui::SetNextWindowSize(kInnerWindowSize);
    ImGui::Begin("inner window", nullptr, kInnerWindowFlag);

    Process();

    ImGui::End();
    ImGuiRnderer::Render();
  }

  ImGuiRnderer::ShutDown();
}

void App::Process() {
  Treat4File();
  if (song_data_.song_len > 0 && song_data_.ch_len > 0) {
    const PositionData position{player_.GetPosition()};
    data_viewer_.DrawSongData(position.table, scroll_frag_);
    DrawLeftPanel();
    ImGui::SameLine();
    DrawRightPanel(position);
  } else if (song_data_.ch_len < 0) {
    ImGui::TextUnformatted("unsupported data");
  }
}

void App::Treat4File() {
  if (ImGui::BeginMainMenuBar()) {
    if (ImGui::BeginMenu("open")) {
      file_dialog_.Open();
      ImGui::EndMenu();
    }
    ImGui::EndMainMenuBar();
  }

  file_dialog_.Display();

  if (file_dialog_.HasSelected()) {
    SDL_PauseAudioDevice(audio_device_, 1);
    play_frag_ = false;
    song_data_ = ModMusic::ReadModFile(file_dialog_.GetSelected());
    if (song_data_.ch_len != 0) {
      player_.Reset();
    }
    file_dialog_.ClearSelected();
  }
}

void App::DrawLeftPanel() {
  ImGui::BeginChild("left panel",
                    ImVec2(Size::kSongDataWidth, Size::kSongDataHeight), true);
  player_viewer_.DrawPlayData();
  ImGui::Spacing();
  ImGui::Separator();
  ImGui::Spacing();
  ImGui::Checkbox("scroll", &scroll_frag_);
  ImGui::Spacing();
  if (play_frag_) {
    if (ImGui::Button("stop", ImVec2(Size::kSongItemWidth, 0.0f))) {
      SDL_PauseAudioDevice(audio_device_, 1);
      play_frag_ = false;
    }
  } else {
    if (ImGui::Button("play", ImVec2(Size::kSongItemWidth, 0.0f))) {
      SDL_PauseAudioDevice(audio_device_, 0);
      play_frag_ = true;
    }
    if (ImGui::Button("next sample", ImVec2(Size::kSongItemWidth, 0.0f))) {
      player_.NextSample();
    }
    if (ImGui::Button("next tick", ImVec2(Size::kSongItemWidth, 0.0f))) {
      player_.NextTick();
    }
    if (ImGui::Button("next row", ImVec2(Size::kSongItemWidth, 0.0f))) {
      player_.NextRow();
    }
    if (ImGui::Button("next frame", ImVec2(Size::kSongItemWidth, 0.0f))) {
      player_.NextFrame();
    }
  }
  ImGui::EndChild();
}

void App::DrawRightPanel(const PositionData& position) {
  ImGui::BeginChild("right panel");
  data_viewer_.DrawPatternData(position.table, position.row, scroll_frag_);
  ImGui::Dummy(ImVec2(Size::kChDummyWidth, 0.0f));
  ImGui::SameLine();
  player_viewer_.DrawChData();
  ImGui::EndChild();
}

}  // namespace ModMusic